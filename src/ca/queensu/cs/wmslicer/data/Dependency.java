/**
 * 
 */
package ca.queensu.cs.wmslicer.data;

import org.eclipse.uml2.uml.Property;

/**
 * @author rezaahmadi
 *
 */
public class Dependency {
	/**
	 * @param dependentNode
	 * @param kind
	 */
	public Dependency(DependencyNode dependentNode, DependencyKind kind, Object what) {
		this.dependentNode = dependentNode;
		this.kind = kind;
		this.what = what;
	}
	
	public Dependency(DependencyNode dependentNode, DependencyKind kind) {
		this.dependentNode = dependentNode;
		this.kind = kind;
	}
	
	public String getWhatStr() {
		return this.what!=null? this.what.toString():"";
//		if (what instanceof Property)
//			return ((Property)what).getName();
//		if (what instanceof Trigger)
//			return ((Property)what).getName();
//		return "";
}
	
	private DependencyNode dependentNode;
	private DependencyKind kind;
	private Object what;
	/**
	 * @return the what
	 */
	public Object getWhat() {
		return what;
	}
	/**
	 * @param what the what to set
	 */
	public void setWhat(Object what) {
		this.what = what;
	}
	/**
	 * @return the node
	 */
	public DependencyNode getDependentNode() {
		return dependentNode;
	}
	/**
	 * @param dependentNode the node to set
	 */
	public void setDependentNode(DependencyNode dependentNode) {
		this.dependentNode = dependentNode;
	}
	/**
	 * @return the kind
	 */
	public DependencyKind getKind() {
		return kind;
	}
	/**
	 * @param kind the kind to set
	 */
	public void setKind(DependencyKind kind) {
		this.kind = kind;
	}
}
