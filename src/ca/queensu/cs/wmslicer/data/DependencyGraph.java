package ca.queensu.cs.wmslicer.data;

/**
 * @author reza
 *
 */

import java.util.List;

import org.eclipse.uml2.uml.Connector;
import org.eclipse.uml2.uml.ConnectorEnd;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.State;
import org.eclipse.uml2.uml.Transition;

import ca.queensu.cs.wmslicer.utils.UmlrtUtil;

import java.util.ArrayList;

public class DependencyGraph extends ArrayList<DependencyNode> {
	
	private String capsuleName;

	/**
	 * @return the capsuleName
	 */
	public String getCapsuleName() {
		return capsuleName;
	}

	/**
	 * @param capsuleName the capsuleName to set
	 */
	public void setCapsuleName(String capsuleName) {
		this.capsuleName = capsuleName;
	}

	public String getNodeByName(String name) {
		for (DependencyNode node: this) {
			if (node.getDependantObj().equals(name))
				return node.toString();
		}
		return null;
	}

	public DependencyNode getNodeOf(Object obj) {
		for (DependencyNode node : this) {
			if (node.getDependant() instanceof State && (State) node.getDependant() == obj)
				return node;
			else if (node.getDependant() instanceof Transition && (Transition) node.getDependant() == obj)
				return node;
			else if (node.getDependant() instanceof String && (String) node.getDependant() == obj)
				return node;
			else if (node.getDependant() instanceof Connector && (Connector) node.getDependant() == obj)
				return node;
			else if (node.getDependant() instanceof Port && (Port) node.getDependant() == obj)
				return node;
			else if (node.getDependant() instanceof org.eclipse.uml2.uml.Class
					&& (org.eclipse.uml2.uml.Class) node.getDependant() == obj)
				return node;
			else if (node.getDependant() instanceof Property && UmlrtUtil.isCapsulePart((Property) node.getDependant())
					&& node.getDependant() == obj)
				return node;
			else if (node.getDependant() instanceof ConnectorEnd
					&& (ConnectorEnd) node.getDependant() == obj)
				return node;
			else System.out.println("Did not found a node for "+obj.toString());
		}
		return null;
	}
	
	public void printAllDependencies() {
		printDependencies();
	}
	
	private void printDependencies() {
		System.out.println("----------------behavioral dependencies----------------");
		for (DependencyNode node : this) {
			if (node.getDependencies() != null && node.getDependencies().size() > 0) {
				// System.out.println(String.format("--[%s] dependencies---",
				// node.getDependantName()));
				if (node.getDependentType() == ObjectType.Transition) {
					// System.out.println("----------------transitions
					// dependencies----------------");
					for (Dependency dep : node.getDependencies()) {
						if (dep.getDependentNode()==null) {
							System.out.println(String.format(""));
						}
						System.out.println(String.format("[%s] ( %s_Dep %s)-> [%s]", node.getDependantName(),
								dep.getKind().toString(),
								dep.getKind() == DependencyKind.dd ? "on " + dep.getWhatStr() : "",
								dep.getDependentNode().getDependantName()));
					}
				}
			}

		}
		System.out.println("----------------structural dependencies----------------");
		for (DependencyNode node : this) {
			if (node.getDependencies() != null && node.getDependencies().size() > 0) {
				// System.out.println(String.format("--[%s] dependencies---",
				// node.getDependantName()));
				if (node.getDependentType() == ObjectType.Port || node.getDependentType() == ObjectType.Part
						|| node.getDependentType() == ObjectType.Connector) { // other types of dependencies
					// System.out.println("----------------structural
					// dependencies----------------");
					for (Dependency dep : node.getDependencies()) {
						// System.out.println(node.getDependantName());
						try {
							System.out.println(String.format("[%s] %s [%s]", node.getDependantName(),
									dep.getKind().toString(), dep.getDependentNode().getDependantName()));
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			}

		}
		
		//action code dependencies
		printActionCodeDependencies();
	}
	
	public void printActionCodeDependencies() {
		System.out.println("----------------action code dependencies----------------");
		for (DependencyNode node : this) {
			if (node.getActionCodes() != null && !node.getActionCodes().isEmpty()) {
				for (Statement ac : node.getActionCodes()) {
					// System.out.println(String.format("--[%s] dependencies---",
					// node.getDependantName()));
					for (Statement dep : ac.getDependencies()) {
						System.out.println(String.format("[%s] ( Dep on %s)-> [%s]", ac.getStatement(),
								dep.getVarAssigned(), dep.getStatement()));
					}
				}
			}
		}
	}
	 
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public List<DependencyNode> getDataOrCtrolDependencies(DependencyGraph dg, List<Transition> transitions, List<String> variables) {
		
//		List<Transition> transitions = new ArrayList<Transition>();
//		List<String> variables = new ArrayList<String>();
		
		List<DependencyNode> res = new ArrayList<DependencyNode>();
		
		for (Transition t : transitions) {
			DependencyNode node = dg.getNodeOf(t);
			
			for (Dependency dep : node.getDependencies()) { // control dep. or data dependency w.r.t. variables
				if (dep.getKind() == DependencyKind.ctrl
						|| (dep.getKind() == DependencyKind.dd && variables.contains(dep.getWhat()))) {
					if (!res.contains(dep.getDependentNode()))
						res.add(dep.getDependentNode());
				}
			}
		}
		
		return res;
	}
	
	public void printMarked() {
		String markedStr = "";
		for (DependencyNode node:this) {
			if (node.getMarked()) {
				markedStr+=node.getDependantName() + " , ";
			}
		}
		System.out.println(String.format("Marked: {%s}", markedStr));
	}
}
