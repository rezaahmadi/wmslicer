/**
 * 
 */
package ca.queensu.cs.wmslicer.data;

/**
 * @author rezaahmadi
 *
 */
public enum CriterionKind {
	Transition, Variable, Trigger, PropertySM, Port, Part
}
