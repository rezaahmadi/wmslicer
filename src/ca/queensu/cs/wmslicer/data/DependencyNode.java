package ca.queensu.cs.wmslicer.data;

import java.util.List;
import java.util.ArrayList;

import org.eclipse.uml2.uml.Connector;
import org.eclipse.uml2.uml.ConnectorEnd;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.State;
import org.eclipse.uml2.uml.Transition;
import org.eclipse.uml2.uml.Vertex;


/**
 * @author reza
 *
 */

public class DependencyNode {
	private Object dependingOnVariable;
	private Object dependingOnMessage;
	private ObjectType objectType;
	private int id;
	private String dependantName;
	private Object dependantObj;
	private Object owner;
	private Boolean marked;
	private List<Dependency> dependencies = new ArrayList<Dependency>();
	private ActionCodes actionCodes;

	/**
	 * @return the actionCodes
	 */
	public ActionCodes getActionCodes() {
		return actionCodes;
	}

	/**
	 * @param actionCodes the actionCodes to set
	 */
	public void setActionCodes(ActionCodes actionCodes) {
		this.actionCodes = actionCodes;
	}

	/**
	 * @return the dependencies
	 */
	public List<Dependency> getDependencies() {
		return dependencies;
	}

	/**
	 * @param dependencies the dependencies to set
	 */
	public void setDependencies(List<Dependency> dependencies) {
		this.dependencies = dependencies;
	}
	
	//List<DependencyNode> dependsOn;
	//DependencyKind dependencyKind;

	public Object getDependingOnVariable() {
		return dependingOnVariable;
	}

	public void setDependingOnVariable(Object dependingOnVariable) {
		this.dependingOnVariable = dependingOnVariable;
	}

	public Object getDependingOnMessage() {
		return dependingOnMessage;
	}

	public void setDependingOnMessage(Object dependingOnMessage) {
		this.dependingOnMessage = dependingOnMessage;
	}

	public Object getOwner() {
		return owner;
	}

	public void setOwner(Object owner) {
		this.owner = owner;
	}
	
//	public DependencyKind getDependencyKind() {
//		return dependencyKind;
//	}

//	public void setDependencyKind(DependencyKind dependencyKind) {
//		this.dependencyKind = dependencyKind;
//	}

	public ObjectType getObjectType() {
		return objectType;
	}

	public void setObjectType(ObjectType objectType) {
		this.objectType = objectType;
	}

	
	/**
	 * @param id
	 * @param dependantName
	 * @param dependantObj
	 * @param marked
	 * @param dependsOn
	 * @param type
	 */
	public DependencyNode(int id, String dependantName, Object dependantObj, Boolean marked,
			List<DependencyNode> dependsOn, DependencyKind dependencyType) {
		super();
		this.id = id;
		this.dependantObj = dependantObj;
		if (dependantObj instanceof Transition)
			dependantName = ((Transition)dependantObj).getName();
		if (dependantObj instanceof Vertex)
			dependantName = ((Vertex)dependantObj).getName();
//		this.dependantName = dependantName;
		this.marked = marked;
		//this.dependsOn = dependsOn;
//		this.dependencyKind = dependencyType;
	}

	public DependencyNode() {
	}
	
	public DependencyNode(int id, ObjectType objectType, Object dependantObj, boolean marked) {
		// TODO Auto-generated constructor stub
		 this(id,objectType, dependantObj,null,marked);
	}
	
	public DependencyNode(int id, ObjectType objectType, Object dependantObj, boolean marked, String dependantName) {
		// TODO Auto-generated constructor stub
		this(id, objectType, dependantObj, null, marked);
		this.dependantName = dependantName;
	}

	public DependencyNode(int id, ObjectType objectType, Object dependantObj, Object container, boolean marked) {
		// TODO Auto-generated constructor stub
		super();
		this.id = id;
		this.objectType = objectType;
		this.dependantObj = dependantObj;
		if (dependantObj instanceof Transition)
			dependantName = ((Transition)dependantObj).getName();
		if (dependantObj instanceof Vertex)
			dependantName = ((Vertex)dependantObj).getName();
		if (dependantObj instanceof Connector)
			dependantName = ((Connector)dependantObj).getName();
		if (dependantObj instanceof Port)
			dependantName = ((Port)dependantObj).getName();
		if (dependantObj instanceof Class)
			dependantName = ((Class)dependantObj).getName();
		if (dependantObj instanceof Property)
			dependantName = ((Property)dependantObj).getName();
		if (dependantObj instanceof ConnectorEnd)
			dependantName =  ((ConnectorEnd) dependantObj).getRole().getName() + "-connectorEnd";
		this.marked = marked;
		//this.dependsOn = new ArrayList<DependencyNode>();
		this.owner = container;
	}
	
		
	public ObjectType getDependentType() {
		return objectType;
	}

	public void setDependentType(ObjectType objectType) {
		this.objectType = objectType;
	}

	/**
	 * @return the dependantName
	 */
	public String getDependantName() {
		return dependantName;
	}
	/**
	 * @param dependantName the dependantName to set
	 */
//	public void setDependantName(String dependantName) {
//		this.dependantName = dependantName;
//	}
	/**
	 * @return the dependantObj
	 */
	public Object getDependantObj() {
		return dependantObj;
	}
	/**
	 * @param dependantObj the dependantObj to set
	 */
	public void setDependantObj(Object dependantObj) {
		this.dependantObj = dependantObj;
	}

	/**
	 * @return the dependantObj
	 */
	public Object getDependant() {
		return dependantObj;
	}
	/**
	 * @param dependant the dependantObj to set
	 */
	public void setDependant(Object dependantObj) {
		this.dependantObj = dependantObj;
	}
	

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the marked
	 */
	public Boolean getMarked() {
		return marked;
	}
	/**
	 * @param marked the marked to set
	 */
	public void setMarked(Boolean marked) {
		this.marked = marked;
	}
	/**
	 * @return the dependsOn
	 */
//	public List<DependencyNode> getDependsOn() {
//		//return dependsOn;
//	}
	/**
	 * @param dependsOn the dependsOn to set
	 */
//	public void setDependsOn(List<DependencyNode> dependsOn) {
//		this.dependsOn = dependsOn;
//	}
	
	@Override
	public boolean equals(Object other) {
		if (other instanceof State)
			return getDependantName().equals(((State) other).getName());
		else if (other instanceof Transition) {
			Transition t1 = (Transition) other;
			Transition t2 = (Transition) dependantObj;
			return (t1.getSource().getName().equals(t2.getSource().getName())
					&& t1.getTarget().getName().equals(t2.getTarget().getName()));
		} else
			return (other.toString().equals(dependantObj.toString()));
	}
}
