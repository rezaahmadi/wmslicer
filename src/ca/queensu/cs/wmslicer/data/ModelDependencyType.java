/**
 * 
 */
package ca.queensu.cs.wmslicer.data;

/**
 * @author rezaahmadi
 *
 */
public enum ModelDependencyType {
	Behaviroal, Structural, ActionCodes
}
