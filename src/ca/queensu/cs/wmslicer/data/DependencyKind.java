/**
 * 
 */
package ca.queensu.cs.wmslicer.data;

/**
 * @author reza
 *
 */
public enum DependencyKind {
	dd, com, ctrl, Timeout, Timer,
	PortOf, PartOf, ConnEnd, con, CommThr, hasPort, hasPart, Triggers, tpd
}