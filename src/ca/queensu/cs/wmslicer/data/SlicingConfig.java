/**
 * 
 */
package ca.queensu.cs.wmslicer.data;

/**
 * @author rezaahmadi
 *
 */
public class SlicingConfig {


	private static String transitions;
	private static String triggers;
	private static String variables;
	private static String propertySmName;
	private static Boolean compositeCapsuleSlicing;
	private static String parts;
	private static String ports;



	public static String getTransitions() {
		// TODO Auto-generated method stub
		return transitions;
	}

	public static String getTriggers() {
		// TODO Auto-generated method stub
		return triggers;
	}

	public static void setTransitions(String text) {
		// TODO Auto-generated method stub
		transitions=text;
	}

	public static void setTriggers(String text) {
		// TODO Auto-generated method stub
	triggers=text;	
	}
 

	public static String getVariables() {
		// TODO Auto-generated method stub
		return variables;
	}

	public static void setVariables(String text) {
		// TODO Auto-generated method stub
		variables=text;
	}

	public static String getPropertySmName() {
		// TODO Auto-generated method stub
		return propertySmName;
	}

	public static void setCompsiteCapsuleSlicing(boolean selected) {
		// TODO Auto-generated method stub
		compositeCapsuleSlicing=selected;
	}
	
	public static boolean getCompsiteCapsuleSlicing() {
		// TODO Auto-generated method stub
		if (compositeCapsuleSlicing==null)
			compositeCapsuleSlicing=false;
		return compositeCapsuleSlicing;
	}

	public static String getParts() {
		// TODO Auto-generated method stub
		return parts;
	}
	
	public static void setParts(String p) {
		// TODO Auto-generated method stub
		 parts = p;
	}
	
	public static String getPorts() {
		// TODO Auto-generated method stub
		return ports;
	}
	
	public static void setPorts(String p) {
		// TODO Auto-generated method stub
		 ports = p;
	}

}
