package ca.queensu.cs.wmslicer.data;

public enum ObjectType {
	State, Transition, Guard, ActionCode, Trigger, CapsuleAttribute, Part, Port, Connector, ConnectorEnd
}
