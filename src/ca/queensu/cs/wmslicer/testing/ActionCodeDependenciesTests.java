/**
 * 
 */
package ca.queensu.cs.wmslicer.testing;

import java.util.stream.Collectors;
import org.junit.Test;

import ca.queensu.cs.wmslicer.core.DependencyCreator;
import ca.queensu.cs.wmslicer.data.*;

/**
 * @author rezaahmadi
 *
 */
public class ActionCodeDependenciesTests {

	@Test
	public void testIsInIfBlocks() {
		ActionCodes ac = new ActionCodes("int Y = 11;\n" + 
				"if (Y>10 && Y<1901)\n" + 
				"{\n" + 
				"    X++;\n" + 
				"    Y--;\n" + 
				"}\n" + 
				"X = X * 2;\n" + 
				"if (X>10){\n" + 
				"    float x = 10;\n" + 
				"    PersonX p2;\n" + 
				"    int x90;\n" + 
				"}");
		
		Statement xx = ac.get(3);
		Statement intY = ac.get(0);
		Statement x = ac.get(6);


		assert ac.stmtIsInIfBlock(xx) != null;
		assert ac.stmtIsInIfBlock(intY) == null;
		assert ac.stmtIsInIfBlock(x) == null;
	}

	//@Test
	//these are to get some output
	public void testPrintDependencies() {
		ActionCodes ac = new ActionCodes("int Y = 11;\n" + 
				"if (Y>10 && Y<1901)\n" + 
				"{\n" + 
				"    X++;\n" + 
				"    Y = Y * X;\n" + 
				"}\n" + 
				"X = X * 2;\n" + 
				"if (X>10){\n" + 
				"    float x = 10;\n" + 
				"    PersonX p2;\n" + 
				"    int x90;\n" + 
				"}");
		
		DependencyCreator dc = new DependencyCreator();
		DependencyGraph dg = new DependencyGraph();
		DependencyNode node = new DependencyNode();
		node.setActionCodes(ac);
		dg.add(node);
		dc.createActionCodeDependencies();
		dg.printActionCodeDependencies();
	}
	
	@Test
	public void testDataDependencies() {
		ActionCodes ac = new ActionCodes("int Y = 11;\n" + 
				"if (Y>10 && Y<1901)\n" + 
				"{\n" + 
				"    X++;\n" + 
				"    Y = Y * X;\n" + 
				"}\n" + 
				"X = X * 2;\n" + 
				"if (X>10){\n" + 
				"    float x = 10;\n" + 
				"    PersonX p2;\n" + 
				"    int x90;\n" + 
				"}");
		
		DependencyCreator dc = new DependencyCreator();
		DependencyGraph dg = new DependencyGraph();
		DependencyNode node = new DependencyNode();
		node.setActionCodes(ac);
		dg.add(node);
		dc.createActionCodeDependencies();
		assert node.getActionCodes().get(4).getDependencies().size() == 3; // if control and two other assigmnet stmts
		assert node.getActionCodes().get(4).getDependencies().stream()
				.filter(dep -> dep.getLine() == 0 || dep.getLine() == 3 || dep.getLine() == 1)
				.collect(Collectors.toList()).size() == 3;
	}
}
