package ca.queensu.cs.wmslicer.testing;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ RegExTests.class, ActionCodeDependenciesTests.class })
public class TestSuit {

}
