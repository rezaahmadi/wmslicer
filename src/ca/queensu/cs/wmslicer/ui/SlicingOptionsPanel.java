package ca.queensu.cs.wmslicer.ui;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import org.eclipse.osgi.container.SystemModule;
import org.eclipse.uml2.uml.Component;

import ca.queensu.cs.wmslicer.core.Slicer;
import ca.queensu.cs.wmslicer.data.*;
import ca.queensu.cs.wmslicer.handlers.Activator;

/**
 * @author reza
 *
 */
public class SlicingOptionsPanel {

	public static class OptionsPanel extends javax.swing.JPanel implements ItemListener {

		private static final long serialVersionUID = 1L;
		// JRadioButton radioBtnPerformCompositeSlicing;
		// JRadioButton radioBtnPerformSimpleSlicing;
		// JRadioButton radioBtnPerformSequentialTestGeneration;
		// JCheckBox chkStopOnFailureFound;
		// JCheckBox chkReduceTestsBasedOnProp;
		//
		ButtonGroup grpOptions = new ButtonGroup();
		JTextField txtVariables;
		JTextField txtTransitions;
		JTextField txtPropertySmName;
		JTextField txtOutMessages;
		JTextField txtParts;
		JTextField txtPorts;

		JCheckBox chkComposite;



		Criteria criteria = new Criteria();
		
		JButton btnSlice;
		JButton btnPrintDependencies;

		JLabel lblVariables;
		JLabel lblTransitions;
		JLabel lblOutMessages;
		JLabel lblCriteria;
		JLabel lblPropertySm;
		private JLabel lblParts;
		private JLabel lblPorts;


		static int order = 0;

		public OptionsPanel() {
			super(new BorderLayout());

			// JOptionPane.showMessageDialog(null, "Hello");
			// radioBtnPerformCompositeSlicing = new JRadioButton ("Perform Slicing on
			// Composite Capsule");
			// radioBtnPerformCompositeSlicing.setSelected(SlicingConfig.isCompositeCapsuleSlicing());
			//
			// radioBtnPerformSimpleSlicing = new JRadioButton ("Perform Slicing on Simple
			// Capsule");
			// radioBtnPerformSimpleSlicing.setSelected(SlicingConfig.isSimpleCapsuleSlicing());

			// chkStopOnFailureFound = new JCheckBox ("Test Generation Stops Once First
			// Failure-producing Test Found");
			// chkStopOnFailureFound.setSelected(TestGenConfig.isStopOnFailureFound());

			// chkReduceTestsBasedOnProp = new JCheckBox ("Reduce Test Cases based on
			// Properties");
			// chkReduceTestsBasedOnProp.setEnabled(radioBtnPerformSymbolicTestGeneration.isSelected());
			// chkReduceTestsBasedOnProp.setSelected(radioBtnPerformSymbolicTestGeneration.isSelected());
			// TestGenConfig.setPropertyAwareTestGeneration(chkReduceTestsBasedOnProp.isSelected());

			// adding all buttons to the same group, so one is selected at a time
			// grpOptions.add(radioBtnPerformCompositeSlicing);
			// grpOptions.add(radioBtnPerformSimpleSlicing);
			// grpOptions.add(radioBtnPerformSymbolicTestGeneration);
			chkComposite = new JCheckBox("Composite capsule slicing");
			chkComposite.setSelected(SlicingConfig.getCompsiteCapsuleSlicing());
			
			txtTransitions = new JTextField();
			txtTransitions.setText(SlicingConfig.getTransitions());

			txtOutMessages = new JTextField();
			txtOutMessages.setText(SlicingConfig.getTriggers());
			
			txtParts = new JTextField();
			txtParts.setText(SlicingConfig.getParts());
			
			txtPorts = new JTextField();
			txtPorts.setText(SlicingConfig.getPorts());

			txtVariables = new JTextField();
			txtVariables.setText(SlicingConfig.getVariables());
			
			txtPropertySmName = new JTextField();
			txtPropertySmName.setText(SlicingConfig.getPropertySmName());
			
			// radioBtnPerformCompositeSlicing.addItemListener(this);
			// radioBtnPerformSimpleSlicing.addItemListener(this);
			// radioBtnPerformSequentialTestGeneration.addItemListener(this);

			// listener
			txtTransitions.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (txtTransitions.getText() != "")
						SlicingConfig.setTransitions(txtTransitions.getText());
				}
			});
			
			lblCriteria = new JLabel("Specify the criteria: ");

			lblTransitions = new JLabel("Transitions: ");
			lblTransitions.setLabelFor(txtTransitions);

			// listener
			txtVariables.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (txtVariables.getText() != "")
						SlicingConfig.setVariables(txtVariables.getText());
				}
			});
			lblVariables = new JLabel("Variables: ");
			lblVariables.setLabelFor(txtVariables);

			// //listener
			txtOutMessages.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (txtOutMessages.getText() != "")
						SlicingConfig.setTriggers(txtOutMessages.getText());
				}
			});
			lblOutMessages = new JLabel("Outgoing messages (\",\" seperated): ");
			lblOutMessages.setLabelFor(txtOutMessages);
			
			// //listener
			txtParts.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (txtParts.getText() != "")
						SlicingConfig.setParts(txtParts.getText());
				}
			});
			lblParts = new JLabel("Parts: ");
			lblParts.setLabelFor(txtParts);
			
			// //listener
			txtPorts.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (txtPorts.getText() != "")
						SlicingConfig.setPorts(txtPorts.getText());
				}
			});
			lblPorts = new JLabel("Ports: ");
			lblPorts.setLabelFor(txtPorts);
			
			// //listener
			txtPropertySmName.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (txtPropertySmName.getText() != "")
						SlicingConfig.setTriggers(txtPropertySmName.getText());
				}
			});
			lblPropertySm = new JLabel("Property SM Name: ");
			lblPropertySm.setLabelFor(txtPropertySmName);

			// slice btn listener
			btnSlice = new JButton("Create slice");
			btnSlice.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					// display/center the jdialog when the button is pressed
					
					//creating criteria
					criteria= new Criteria();
//					if (txtVariables.getText()!="")
//						criteria.add(new Criterion(txtVariables.getText(), CriterionKind.Variable));
//					if (txtTransitions.getText()!="")
//						criteria.add(new Criterion(txtTransitions.getText(), CriterionKind.Transition));
					System.out.println("criteria is below");
					System.out.println("transition:"+ txtTransitions.getText());
					System.out.println("variable:"+ txtVariables.getText());
					System.out.println("outgoing messages:"+ txtOutMessages.getText());
					System.out.println("ports:"+ txtPorts.getText());
					System.out.println("parts:"+ txtParts.getText());
					
					if (txtPorts.getText().length() > 0) {
						String[] ports = txtPorts.getText().split(",");
						for (String port : ports) {
							criteria.add(new Criterion(port, CriterionKind.Port));
						}
					}
					if (txtParts.getText().length() > 0) {
						String[] parts = txtParts.getText().split(",");
						for (String part : parts) {
							criteria.add(new Criterion(part, CriterionKind.Part));
						}
					}
					if (txtOutMessages.getText().length() > 0) {
						String[] outMessages = txtOutMessages.getText().split(",");
						for (String outMsg : outMessages) {
							criteria.add(new Criterion(outMsg, CriterionKind.Trigger));
						}
					} 
					if (txtVariables.getText().length() > 0 && txtTransitions.getText().length() > 0) {
						criteria.add(new Criterion(txtVariables.getText(), CriterionKind.Variable));
						criteria.add(new Criterion(txtTransitions.getText(), CriterionKind.Transition));
					}
					if (txtPropertySmName.getText().length() > 0) {
						criteria.add(new Criterion(txtPropertySmName.getText(), CriterionKind.PropertySM));
					}
					long time1 = System.currentTimeMillis();
					Slicer.slice(Activator.getSelectedCapsule(), criteria);
					long time2 = System.currentTimeMillis() - time1;
					System.out.println ( String.format( "******* time to slice: %s (s), %s (ms) ***********", time2/1000, time2));
				}
			});
			
			// generate dependency graph
			btnPrintDependencies = new JButton("Show Dependency graph");
			btnPrintDependencies.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					// display/center the jdialog when the button is pressed
					long time1 = System.currentTimeMillis();
					long mem1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
					Slicer.createAndPrintDependencies(Activator.getSelectedCapsule());
					long time2 = System.currentTimeMillis() - time1;
					long mem2 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
					System.out.println(String.format(
							"******* time to create dependencies: %s (s), %s (ms)--memory usage:%sKB***********",
							time2 / 1000, time2, (mem2 - mem1) / 1024));
				}
			});
			
			//Composite capsule slicing 
			chkComposite.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					SlicingConfig.setCompsiteCapsuleSlicing(chkComposite.isSelected());
				}
			});

			// generate dependency graph
			// radioBtnPerformCompositeSlicing.addActionListener(new ActionListener() {
			// public void actionPerformed(ActionEvent e) {
			// if (radioBtnPerformCompositeSlicing.isSelected()) {
			// SlicingConfig.setPerformCompositeSlicing(true);
			// }else
			// {
			// SlicingConfig.setPerformCompositeSlicing(false);
			// }
			// }
			// });

			// Put the check boxes in a column in a panel
			JPanel checkPanel = new JPanel(new GridLayout(0, 2));
			// checkPanel.add(radioBtnPerformRandomTestGeneration);
			// checkPanel.add(radioBtnPerformSequentialTestGeneration);
			// checkPanel.add(radioBtnPerformSymbolicTestGeneration);
			// checkPanel.add(chkReduceTestsBasedOnProp);
			// checkPanel.add(chkStopOnFailureFound);
			//checkPanel.add(lblCriteria);
			checkPanel.add(lblTransitions);
			checkPanel.add(txtTransitions);
			checkPanel.add(lblVariables);
			checkPanel.add(txtVariables);
			checkPanel.add(lblOutMessages);
			checkPanel.add(txtOutMessages);
			checkPanel.add(lblParts);
			checkPanel.add(txtParts);
			checkPanel.add(lblPorts);
			checkPanel.add(txtPorts);
			checkPanel.add(lblPropertySm);
			checkPanel.add(txtPropertySmName);
//			checkPanel.add(chkComposite);
			checkPanel.add(btnPrintDependencies);
			checkPanel.add(btnSlice);
			add(checkPanel, BorderLayout.LINE_START);
			setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
		}

		@Override
		public void itemStateChanged(ItemEvent e) {
			// TODO Auto-generated method stub

		}
	}

	private void createAndShowGUI() {
		// Create and set up the window.
		// JDialog frame = new JDialog("Configuring test case generation");
		JFrame frame = new JFrame("<< Specify a criteria to slice a UML-RT Capsule >>");
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		// Create and set up the content pane.
		JComponent newContentPane = new OptionsPanel();
		newContentPane.setOpaque(true); // content panes must be opaque
		frame.setContentPane(newContentPane);

		// trigger textbox actionPerformed
		// JButton b;
		// for (Component c: newContentPane.getComponents()){
		// if (c instanceof JButton){
		// b = (JButton)c;break;
		// }
		// }
		// frame.getRootPane().setDefaultButton(btnGenerateTests);
		// //adjust location
		// Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		// frame.setLocation(dim.width/2-frame.getSize().width/2,
		// dim.height/2-frame.getSize().height/2);

		// Display the window.
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}

	public void startGUI() {
		// LocalCheckBoxes cbs = new LocalCheckBoxes();
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}
