/**
 * 
 */
package ca.queensu.cs.wmslicer.ui;

import java.util.Map;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import java.util.*;
import java.awt.event.*;


/**
 * @author rezaahmadi
 *
 */

// This library uses mxgraph.JGraph to visualize 
// dependencies in a UML-RT Capsule

import javax.swing.JFrame;
import com.mxgraph.layout.mxFastOrganicLayout;

//import org.eclipse.swt.events.MouseAdapter;
//import org.eclipse.swt.events.MouseEvent;
//import org.eclipse.swt.events.MouseListener;

import com.mxgraph.model.mxCell;
import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.util.mxConstants;
import com.mxgraph.view.mxGraph;

import ca.queensu.cs.wmslicer.data.Dependency;
import ca.queensu.cs.wmslicer.data.DependencyGraph;
import ca.queensu.cs.wmslicer.data.DependencyNode;

public class DependencyViewer extends JFrame {

	private static final long serialVersionUID = 12L;
//	mxCell cell = null;

	private GraphControl graphControl;

	private DependencyViewer(DependencyGraph dg) {
		super(String.format( "<< Dependency Viewer, Capsule: %s >>", dg.getCapsuleName()));

		// super("mxGraph");

		// Creates graph with model
		mxGraph graph = new mxGraph();
		//graph.setResetEdgesOnMove(true);
		graph.setGridEnabled(false);
		Object parent = graph.getDefaultParent();

		// Sets the default vertex style
		Map<String, Object> style = graph.getStylesheet().getDefaultVertexStyle();
		style.put(mxConstants.STYLE_GRADIENTCOLOR, "#AB22EE");
		style.put(mxConstants.STYLE_ROUNDED, true);
		style.put(mxConstants.STYLE_SHADOW, true);
		style.put(mxConstants.STYLE_NOEDGESTYLE, false);
		style.put(mxConstants.STYLE_FONTCOLOR, "#000000");
//		style.put(mxConstants.STYLE_EDGE, mxEdgeStyle.ElbowConnector);
		style.put(mxConstants.STYLE_STROKECOLOR, "#000FFF");


		//style.put(mxConstants.STYLE_LABEL_BORDERCOLOR, "#000000");
		//style.put(mxConstants.STYLE_LABEL_BACKGROUNDCOLOR, "#000000");


		Map<Object, Object> Dependency_Vertex_Map = new HashMap<Object, Object>();

		graph.getModel().beginUpdate();

		try {

			//adding new vertex per node
			for (DependencyNode node : dg) {
				if (node.getDependencies() != null && node.getDependencies().size() > 0) {
					Object vertex = graph.insertVertex(parent, null, node.getDependantName(), 0, 0, 60, 30);
					// if (!Dependency_Vertex_Map.containsKey(node.getDependant()))
					Dependency_Vertex_Map.put(node, vertex);
				}
			}
			
//			if (Dependency_Vertex_Map.size()<2) { //dum
				
				
			// add nodes
			for (DependencyNode node : dg) {

				List<Dependency> dependencies = node.getDependencies();

				// only add nodes with dependencies
				// if (dependencies != null && dependencies.size() > 0) {
				// source node
				Object nodeFrom = Dependency_Vertex_Map.get(node); 
				for (Dependency dep : dependencies) {

					// target node
					Object nodeTo = Dependency_Vertex_Map.get(dep.getDependentNode()); 

					// create edge
					Object newEdge = graph.insertEdge(parent, null, dep.getKind(), nodeFrom, nodeTo);
					
					//triangle
//					mxCell v3 = (mxCell) graph.insertVertex(newEdge, null, dep.getKind(), -0.5, 0, 40, 40,
//							"shape=triangle");
//					v3.getGeometry().setRelative(true);
//					v3.getGeometry().setOffset(new mxPoint(-20, -20));

				}
				// }
			}
			
//			} //dum

            //mxIGraphLayout layout = new ExtendedCompactTreeLayout(graph);
			
			
			
		} finally {
			graph.getModel().endUpdate();
		}

		// Creates a control in a scrollpane
//		graphControl = new GraphControl(graph);
         mxGraphComponent graphComponent = new mxGraphComponent(graph);
         getContentPane().add(graphComponent);
//		JScrollPane scrollPane = new JScrollPane(graphControl);
//		scrollPane.setAutoscrolls(true);
		
		//adding mouse listen event to the graph control
         graphComponent.getGraphControl().addMouseListener(new MouseAdapter() {
//			int X,Y;
//			mxCell cell = null;
//			@Override
//			public void mousePressed(MouseEvent e) {
//				cell = (mxCell) graphComponent.getCellAt(e.getX(), e.getY());
//				System.out.println("pressed: X, Y: " + e.getX() + " " + e.getY());
//				X=e.getX();
//				Y=e.getY();
//			}
//			@Override
			public void mouseReleased(MouseEvent e) {
				mxCell cell = (mxCell) graphComponent.getCellAt(e.getX(), e.getY());
				if (cell != null) {
					System.out.println("released  : X, Y: " + e.getX() + " " + e.getY());
//				    graph.getModel().beginUpdate();
//					graph.moveCells(new Object[] { cell }, e.getX(), e.getY(), false);
//					graph.getModel().endUpdate();
				}
			}
		});
         
       //layout
		mxFastOrganicLayout layout = new mxFastOrganicLayout (graph);// new mxCompactTreeLayout(graph);// mxHierarchicalLayout(graph);
//         mxHierarchicalLayout layout = new mxHierarchicalLayout (graph);// new mxCompactTreeLayout(graph);// mxHierarchicalLayout(graph);

         layout.setUseBoundingBox(false);
		layout.execute(parent);
		
	

		// Puts the control into the frame
//		setLayout(new BorderLayout());
//		add(scrollPane, BorderLayout.CENTER);
//		setPreferredSize(new Dimension(720, 720));
		
		
		
		//show menu bar
		createMenuBar();
		
	}

	public static void showUI(DependencyGraph dg) {
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {

//				JFrame frame = new JFrame("<< Dependency Viewer >>");
//				frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

				// Create and set up the content pane.
				JFrame dv = new DependencyViewer(dg);
				dv.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
//				newContentPane.setOpaque(true); // content panes must be opaque
//				frame.setContentPane(newContentPane);

				// Display the window.
//				frame.pack();
				dv.setLocationRelativeTo(null);
				dv.setVisible(true);

			}
		});
	}
	
	private static void exportPdf() {
//		mxRectangle bounds = graph.getGraphBounds();
//		Document document = new Document(new Rectangle((float) (bounds
//				.getWidth()), (float) (bounds.getHeight())));
//		PdfWriter writer = PdfWriter.getInstance(document,
//				new FileOutputStream("example.pdf"));
//		document.open();
//		final PdfContentByte cb = writer.getDirectContent();
//
//		mxGraphics2DCanvas canvas = (mxGraphics2DCanvas) mxCellRenderer
//				.drawCells(graph, null, 1, null, new CanvasFactory()
//				{
//					public mxICanvas createCanvas(int width, int height)
//					{
//						Graphics2D g2 = cb.createGraphics(width, height);
//						return new mxGraphics2DCanvas(g2);
//					}
//				});
//
//		canvas.getGraphics().dispose();
//		document.close();
	}
	
	private static void search() {
		
	}
	 private void createMenuBar() {

	        JMenuBar menubar = new JMenuBar();
//	        ImageIcon exitIcon = new ImageIcon("src/main/resources/exit.png");

	        //export items
	        JMenu export = new JMenu("Export");
	        export.setMnemonic(KeyEvent.VK_E);
	        
	        JMenuItem exportMenuItem = new JMenuItem("Export to pdf");
	        exportMenuItem.setMnemonic(KeyEvent.VK_E);
	        exportMenuItem.setToolTipText("export to pdf file");
	        exportMenuItem.addActionListener((ActionEvent event) -> {
	        		exportPdf();
	        });
	        
	        //search items
	        JMenu search = new JMenu("Search");

	        search.setMnemonic(KeyEvent.VK_S);

	        JMenuItem searchMenuItem = new JMenuItem("Find in the graph");
	        searchMenuItem.setMnemonic(KeyEvent.VK_E);
	        searchMenuItem.setToolTipText("search in the graph");
	        searchMenuItem.addActionListener((ActionEvent event) -> {
	        		search();
	        });


	        export.add(exportMenuItem);
	        search.add(searchMenuItem);
	        

	        menubar.add(export);
	        menubar.add(search);


	        setJMenuBar(menubar);
	    }

}
