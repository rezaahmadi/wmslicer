package ca.queensu.cs.wmslicer.core;

import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Pseudostate;
import org.eclipse.uml2.uml.Transition;
import org.eclipse.uml2.uml.Vertex;

import ca.queensu.cs.wmslicer.data.ActionCodes;
import ca.queensu.cs.wmslicer.data.Criteria;
import ca.queensu.cs.wmslicer.data.Criterion;
import ca.queensu.cs.wmslicer.data.CriterionKind;
import ca.queensu.cs.wmslicer.data.Dependency;
import ca.queensu.cs.wmslicer.data.DependencyGraph;
import ca.queensu.cs.wmslicer.data.DependencyNode;
import ca.queensu.cs.wmslicer.data.Statement;
import ca.queensu.cs.wmslicer.ui.DependencyViewer;
import ca.queensu.cs.wmslicer.utils.*;

import java.util.*;

/*
 * Our slicing algorithm (without timers) is partly based on B.Korel's slicing on EFSM
 * We have added supporting models with timers and communicating state machines on top of the existing work
 */

/**
 * @author reza
 * 
 *         Our slicing technique is partly based on Korel et.al algorithm to identify 
 *         and slice dependencies in a state machine and the rest are based
 *         on different types of dependencies that we identify in a UML-RT model
 */

public class Slicer {

	static DependencyGraph dg;

	@SuppressWarnings("unused")
	public static Class slice(Class capsule, Criteria criteria) {
		long time1 = System.currentTimeMillis();
		System.out.println("slicing starte. recording time..");

		
		if (criteria == null || criteria.size() == 0)
			return capsule;
		
		//Resource rs = UmlrtUtil.loadSlicingUnitTestModels();

		// Class prop = capsule;

		// dg = new DependencyGraph();

		// DependencyGraph dg = new DependencyGraph();

		// StateMachine criteriaSm = UmlrtUtil.getStateMachine(prop);
		// StateMachine cutSm = UmlrtUtil.getStateMachine(capsule);
		// List<String> criteria = new ArrayList<String>();

		// markDependencyGraph(criteria, dg);

		// createAndPrintDependencies(capsule);

		// 4
		// compute dependence graph
		// dependency graph
		DependencyCreator dc = new DependencyCreator(capsule);
		dc.createStructuralDependencies(true, capsule, UmlrtUtil.getCapsuleParts(capsule));
		dc.createBehavioralDependencies();

		DependencyGraph dg = dc.getDependencyGraph();

		//apply transactional semantics to reading and writing the contents of an EMF object
		TransactionalEditingDomain domain = TransactionUtil.getEditingDomain(capsule);
		domain.getCommandStack().execute(new RecordingCommand(domain) {
			@Override
			protected void doExecute() {
				
				//list all transitions to be used throughout the process
				List<Transition> allTransitions = new ArrayList<Transition>();
				for (Object o : dc.transtates) {
					if (o instanceof Transition)
						allTransitions.add((Transition) o);
				}
				
				// slicing based on criteria
				// 0
				// mark all as non-contributing
				for (DependencyNode node : dg) {
					node.setMarked(false);
				}
				
				//mark the capsule contributing
				//dg.getNodeOf(capsule).setMarked(true);
				
				dg.printMarked();
				
				
				// 1
				// mark outgoing transition from initial state
				//since we need entry point to the state machine
				Vertex initialState = UmlrtUtil.getInitialState(UmlrtUtil.getStateMachine(capsule));
				if (initialState != null) {
					List<Transition> outsFromInit = initialState.getOutgoings();
					if (outsFromInit != null && outsFromInit.get(0) != null) {
						DependencyNode initNode = dg.getNodeOf(outsFromInit.get(0));
						if (initNode != null) {
							initNode.setMarked(true);
						}
					}
				}
				
				dg.printMarked();
				
				
				// ToDo: for the moment we support triggers and variables as criterion
				// criteria.removeEnpties();
				// if (criteria.get(0).getKind() == CriterionKind.Trigger) {

				// 2
				// find transitions that generates the trigger(s) in the crieria
				if (criteria.hasCriterionOfKind(CriterionKind.Trigger)) {
					List<Transition> trigGenerators = new ArrayList<Transition>();
					for (Transition t : allTransitions) {
						for (Criterion criterion : criteria) {
							// trigger is generated transition t
							if (RegExUtil.isTriggerGenerated(UmlrtUtil.getActionCodeStr(t),
									(String) criterion.getValue())) {
								trigGenerators.add(t);
							}
						}
					}
					// add trigGenerators transitions to the criteria
					for (Transition t : trigGenerators) {
						criteria.add(new Criterion(t, CriterionKind.Transition));
					}
				}
				
				//2
				//ToDo fix this B.S.
				if (criteria.hasCriterionOfKind(CriterionKind.Transition)) {
					for (Criterion c : criteria) {
						if (c.getKind()== CriterionKind.Transition) {
							for (Transition t : allTransitions) {
								if (t.getName().equals(c.getValue()))
									c.setValue(t);
							}
						}
					}
				}
				
				/*				
				// 3
				// delete all transitions from which there is no path to transitions in the criteria
				// transitions
				// first find all path from the source state of each transition T
				// to the destination of each transitions in trigGenerators
//				List<Transition> allTransitions = UmlrtUtil.getAllTransitions(capsule);
				System.out.println("removing all transitions from which there is no path to transitions in the criteria");
				List<Transition> transitionsInCriteria = criteria.getTransitions();
				int c = 0;
				while (c < allTransitions.size()) {
					Transition t = allTransitions.get(c);
					// we do not include choice points out/in transitions
					if (!UmlrtUtil.isChoicePoint(t.getTarget()) && !UmlrtUtil.isChoicePoint(t.getSource())) {
						int p = 0;
						boolean notCriteria = false;
						for (Transition tc : transitionsInCriteria) {
							if (!t.equals(tc)) {
								notCriteria = true;
								List<TransitionPath> allPaths = new ArrayList<TransitionPath>();
								TransitionPath currentPath = new TransitionPath();
								if (t.getTarget().equals(tc.getSource())) {// there for sure exist a path in this case
									p++;
									break;
								} else
									UmlrtUtil.findAllPaths(t.getTarget(), tc.getSource(), currentPath, allPaths);
								if (allPaths.size() > 0) {
									p++;
									break;
								}
							}
						} // for
						if (p < 1 && notCriteria) {
							// remove the transition from the state machine
							// ToDo:fix this and uncomment it
							UmlrtUtil.removeTransition(UmlrtUtil.getStateMachine(capsule), t);
						}
					} // if
					c++;
				} // while
				
*/
				
				// 5
				// mark trigGenerators as contributing
				for (Transition t : criteria.getTransitions()) {
					dg.getNodeOf(t).setMarked(true);
				}

				dg.printMarked();
				

				// 6
				// any variables used in guards of transitions criteria are added to the variables
				// criteria
				for (Transition t : criteria.getTransitions()) {
					for (String var : RegExUtil.getAllVarsUsed(UmlrtUtil.getGuardStr(t))) {
						//check if the variable to be added is a capsule attribute
						if (UmlrtUtil.isCapsuleProperty(capsule, var))
							criteria.add(new Criterion(var, CriterionKind.Variable));
					}
				}
				
				
				// 6.5
				//
				// 1.use action code statement control & data dependencies to 
				// add any other variable dependencies to the criteria.
				// 2.any statement of the action code that a variables in the criteria is dependent on 
				// is marked.
				if (criteria.hasCriterionOfKind(CriterionKind.Variable)) {
					//updateCriteriaBasedOnActionCode(dg, UmlrtUtil.getActionCodeStr(t), criteria);
					Transition t = criteria.getTransitions().get(0);
					String varName = criteria.getVariables().get(0);
					
					DependencyNode node = dg.getNodeOf(t);
					Statement stmt = node.getActionCodes().getByVarAssignedName(varName);
					markActionCodeStatements_UpdateCriteria(stmt, criteria);
					node.getActionCodes().printMarked();
				}

				// 7
				//
				// 1.find all the nodes that are data dependent to variables in the criteria
				// ToDo: in each statement in each action code, there can be more dependencies that 
				// must be added to the criteria
				List<DependencyNode> initialNodes = dg.getDataOrCtrolDependencies(dg, criteria.getTransitions(),
						criteria.getVariables());
				//adding all transitions in the criteria to initialNodes
				for (Transition t:criteria.getTransitions())
					initialNodes.add(dg.getNodeOf(t));
				
				// 2.for all the dependencies found, we need to mark statements that 
				// the variables in the criteria are dependent on
				if (criteria.hasCriterionOfKind(CriterionKind.Variable)) {
					// updateCriteriaBasedOnActionCode(dg, UmlrtUtil.getActionCodeStr(t), criteria);
					for (DependencyNode node : initialNodes) {
						for (Statement stmt : node.getActionCodes()) {
							if (criteria.getVariables().stream().anyMatch(varName -> !varName.isEmpty() && varName.equals(stmt.getVarAssigned()))) {
								markActionCodeStatements_UpdateCriteria(stmt, criteria);
								// node.getActionCodes().printMarked();
							}
						}
					}
				}
				
				// 3.Traverse Backward in dependence graph
				// for each transition T for which there is a data dependency w.r.t.
				// any variables in Criteria or control dependencies with transitions in the criteria
				// mark T
				for (DependencyNode node:initialNodes)
					node.setMarked(false);
				TraverseBackwardDG(initialNodes);

				dg.printMarked();
				


				// 8
				// ToDo: here i probably need to mark conditional transitions as well.
				// preserving transitions to and from pseudo states 
				// (and transitions that these have dependency on) in case
				// any transition connected to these states was marked before
				for (Pseudostate psudo : UmlrtUtil.getPseudostates(capsule)) {
					List<Transition> psodoTrans = new ArrayList<Transition>();
					List<DependencyNode> psudoRelatedNodes = new ArrayList<DependencyNode>();
					
					psodoTrans.addAll(psudo.getOutgoings());
					psodoTrans.addAll(psudo.getIncomings());
					Transition tPsodo = null;
					for (Transition t : psodoTrans) { 
						DependencyNode node = dg.getNodeOf(t);
						if (node.getMarked()) {//at least one transition connected to a choice is marked
							tPsodo = t;
							break;
						}
					}
					if (tPsodo!=null) {
						psodoTrans.remove(tPsodo);
						for (Transition t : psodoTrans) {
							DependencyNode node = dg.getNodeOf(t);
							//ToDo: here we need to find all other transitions that 
							//are data or control dependents on the above nodes as well
							//node.setMarked(true);
							node.setMarked(false);
							psudoRelatedNodes.add(node);
						}
						
						//mark transitions that psodoTrans are dependent on, as well
						TraverseBackwardDG(psudoRelatedNodes);
					}
				}
				
				dg.printMarked();
				


				// 9
				// remove non-contributing (unmarked) elements
				System.out.println("removing non-contributing (unmarked) elements");
				for (DependencyNode node : dg) {
					Object dependent = node.getDependant();
					if (!node.getMarked() && dependent != null && dependent!=capsule) { //do not remove the main capsule
						// if (!node.getMarked() && dependent instanceof Transition) {
						System.out.println("removing " + node.getDependantName() + "...");
						UmlrtUtil.removeElement(capsule, dependent);
						// }
					}
				}
				// remove idle transitions, in this case self-transitions with no effect
				for (Transition t : UmlrtUtil.getSelfTransitions(capsule)) {
					if (UmlrtUtil.getActionCodeStr(t).equals("")) {
						UmlrtUtil.removeTransition(capsule, t);
					}
				}
				
				// remove states with no incoming and outgoing transitions
				System.out.println("removing states with no incoming and outgoing transitions");
				List<Vertex> delVertexes = new ArrayList<Vertex>();
				for (Vertex v:UmlrtUtil.getAllVertexes(capsule)) {
					if (!UmlrtUtil.isCompositeState(v) && UmlrtUtil.hasEmptySm(v) && (v.getOutgoings() == null || v.getOutgoings().size() == 0)
							&& (v.getIncomings() == null || v.getIncomings().size() == 0)) {
						//UmlrtUtil.removeVertex(capsule, v);
						delVertexes.add(v);
						System.out.println("removing "+(v).getName());
					}
				}
				UmlrtUtil.removeVertexs(capsule, delVertexes);
				
				
				//remove unmarked statements in action codes
				//ToDo: support states as well
				//ToDo: this must be done for all the nodes in the dg
				System.out.println("removing unmarked statements in the action code");
				if (criteria.hasCriterionOfKind(CriterionKind.Variable)) {
					// updateCriteriaBasedOnActionCode(dg, UmlrtUtil.getActionCodeStr(t), criteria);

					//ToDo: this should be done for all the nodes in dg
					for (DependencyNode node : initialNodes) {
						ActionCodes clonedAc = node.getActionCodes().clone();
						for (int i = 0; i < clonedAc.size(); i++) {
							Statement stmt = clonedAc.get(i);
							if (!stmt.isMarked()) {
								node.getActionCodes().remove(stmt);
							}
						}
						// updating the action code
						UmlrtUtil.setNewEffect((Transition)node.getDependant(), node.getActionCodes().getActionCodesStr());

						node.getActionCodes().printAll();
					}
				}

				dg.printMarked();
				
			}// doExecute

		});
		
		long time2 = System.currentTimeMillis();
		
		System.out.println(String.format("time to calculate the slice: %d", time2 - time1));
		
		return capsule;
	}
	
	private static void markActionCodeStatements_UpdateCriteria(Statement stmt, Criteria criteria) {
		if (criteria.getVariables().stream().anyMatch(var -> var.equals(stmt.getVarAssigned()))) {
			stmt.setMarked(true);
			//criteria.add(new Criterion(stmt.getVarAssigned(), CriterionKind.Variable));
			for (Statement dep : stmt.getDependencies()) {
				criteria.add(new Criterion(dep.getVarAssigned(), CriterionKind.Variable));
				markActionCodeStatements_UpdateCriteria(dep, criteria);
			}
		}
	}
	
	private static void TraverseBackwardDG(List<DependencyNode> initialNodes) {
		// in the dependency graph mark those nodes which have either:
		// control dependency with the transitions in the criteria
		// or data dependency with those transitions w.r.t. the variables in the
		// criteria
		for (DependencyNode node : initialNodes) {
			if (!node.getMarked()) {
				node.setMarked(true);
				List<DependencyNode> nodes = new ArrayList<DependencyNode>();
				for (Dependency dep : node.getDependencies()) {
					nodes.add(dep.getDependentNode());
				}
				TraverseBackwardDG(nodes);
			} // if
		} // for
	}
	
	// detect data and control dependencies in the action code
	// and unmark lines with no dependencies
	// unmarked lines will not be part of the final slice
	// ToDo:fix this
	private void markActionCodeStatements(ActionCodes acs) {
		// for (Statement stmt : acs) {
		// if (vars.stream().anyMatch(var ->
		// RegExUtil.isVarAssigned(stmt.getStatement(), var))) {
		// stmt.setMarked(true);
		// }
		// }
		
		//control dependencies
//		for (Statement stmt : node.getActionCodes().stream().filter(ac -> ac.isMarked())
//				.collect(Collectors.toList())) {
//			for (int location : node.getActionCodes().getRelatedIfLocations(stmt))
//				node.getActionCodes().setMarked(location);
//		}
	}
	
	public static void createAndPrintDependencies(Class cut){
		// 4
		// compute dependence graph
		// dependency graph
		DependencyCreator dc = new DependencyCreator(cut);
		dc.createBehavioralDependencies();
		dc.createStructuralDependencies(true, cut, UmlrtUtil.getCapsuleParts(cut));
		DependencyGraph dg = dc.getDependencyGraph();
		
		dg.printAllDependencies();
		
		DependencyViewer.showUI(dg);
	}

//	public static void createAndPrintDependencies_old(Class cut) {
//		
//		dg = new DependencyGraph();
//		
//		DependencyCreator dc = new DependencyCreator(cut);
//		
//		dc.initDependencyGraph(dg);
//
//		//data
//		dc.createDataDependencies(dg);
//		
//		//maximal paths
//		
//		List<TransitionPath> allPaths = new ArrayList<TransitionPath>();
//		TransitionPath currentPath = new TransitionPath();
//		UmlrtUtil.findAllMaximalPaths(UmlrtUtil.getState(cut, "S2"), UmlrtUtil.getState(cut, "S2"), currentPath, allPaths );
//		
//		for (TransitionPath tp:allPaths) {
//			tp.print();
//		}
//		//control
//		dc.createControlDependencies(dg);
//		
//		//communication
//		dc.createCommDependency(dg);
//		
//		dg.printAllDependencies();
//	}
	

//	public static void printDG() {
//		// TODO Auto-generated method stub
//		dg.printAllDependencies();
//	}
	
//	public static void testCtrlD(Class cut) {
//		DependencyCreator2 dc = new DependencyCreator2(cut);
//		
//		DependencyGraph dg = new DependencyGraph();
//
//		dc.initDependencyGraph(dg);
//
//		dc.createControlDependencies(dg);
//		dg.printAllDependencies();
//	}

	//ToDo fix this
//	private static void markDependencyGraph(List<String> criteria, DependencyGraph dg) {
//		//marking the dependency graph nodes based on the criteria
//				
//	}

}
