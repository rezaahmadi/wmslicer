package ca.queensu.cs.wmslicer.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.IWorkbenchWindowActionDelegate;
import org.eclipse.uml2.uml.Class;

import ca.queensu.cs.wmslicer.ui.SlicingOptionsPanel;
import ca.queensu.cs.wmslicer.utils.UmlrtUtil;

/**
 * @author reza implements and extends both interface and base cases to allow
 *         user generating test cases using popup menu or using toolbar. ToDo:
 *         AbstractHandler.execute is not implemented yet.
 */
public class Activator extends AbstractHandler implements IWorkbenchWindowActionDelegate {

	private static Class selectedCapsule;
	private static Class cut;
	
	private static IWorkbenchWindow window;

	 
	@Override
	public void run(IAction arg0) {
		if (getSelectedCapsule() != null) {
			//StateMachineSlicer.testDD(getSelectedCapsule());
			new SlicingOptionsPanel().startGUI();
		}
	}

	@Override
	public void selectionChanged(IAction action, ISelection selection) {
		// TODO Auto-generated method stub
		if (!(selection instanceof IStructuredSelection)) {
			return;
		}
		IStructuredSelection structured = (IStructuredSelection) selection;

		Object selectedObject = structured.getFirstElement();

		if (selectedObject instanceof IAdaptable) {
			IAdaptable adaptableObject = (IAdaptable) selectedObject;
			setSelectedCapsule(UmlrtUtil.selectIfCapsule((EObject) adaptableObject.getAdapter(EObject.class)));
			cut = UmlrtUtil.getCapsuleByStereotypeName(getSelectedCapsule(), "CapsuleUnderTest");
		}
	}

	@Override
	public void dispose() {
	}

	@Override
	public void init(IWorkbenchWindow w) {
		this.window = w;
	}

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		// TODO Auto-generated method stub
		return null;
	}

	public static Class getSelectedCapsule() {
		return selectedCapsule;
	}

	public static void setSelectedCapsule(Class selectedCapsule) {
		Activator.selectedCapsule = selectedCapsule;
	}

}
