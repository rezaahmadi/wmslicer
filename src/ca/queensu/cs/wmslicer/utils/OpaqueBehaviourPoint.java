package ca.queensu.cs.wmslicer.utils;

/**
 * @author reza
 *
 */
public enum OpaqueBehaviourPoint {
	OnExit,
	OnEntry
}
